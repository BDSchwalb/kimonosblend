package com.kimonosblend.app.kimonosblend;

import android.os.Bundle;

import com.kimonosblend.app.strategy.ClothingAllStrategy;
import com.kimonosblend.app.strategy.ClothingNarrowStrategy;

public class WardrobeFragment extends DisplayClothesFragment {

    private ClothingNarrowStrategy mStrategy = new ClothingAllStrategy();

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment WardrobeFragment.
     */
    public static WardrobeFragment newInstance() {
        WardrobeFragment fragment = new WardrobeFragment();
        return fragment;
    }

    @Override
    public ClothingNarrowStrategy getNarrowStrategy() {
        return mStrategy;
    }
}
