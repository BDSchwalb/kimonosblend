package com.kimonosblend.app.networking;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class RetrievePictureRequest implements ClientRequest {

    private static final String Tag = RetrievePictureRequest.class.getName();

    private static final String IdKey = "id";

    private String mPictureId;

    public RetrievePictureRequest(String picId) {
        mPictureId = picId;
    }

    @Override
    public JSONObject createPayload() {
        JSONObject object = new JSONObject();
        try {
            object.put(IdKey, mPictureId);
        } catch (JSONException e) {
            Log.d(Tag, "Unable to create JSON object");
        }

        return object;
    }
}
