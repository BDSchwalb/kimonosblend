package com.kimonosblend.app.networking;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.ByteBuffer;

public class RetrievePictureResponse {

    private static final String Tag = RetrievePictureResponse.class.getName();

    private static final String ValidKey = "valid";
    private static final String DataKey = "data";

    private boolean mError;
    private boolean mValid;
    private ByteBuffer mData;

    public RetrievePictureResponse(JSONObject object) {
        mError = false;
        mValid = false;
        mData = null;

        if (object != null) {
            try {
                if (!object.has(ValidKey) || object.getBoolean(ValidKey)) {
                    mValid = true;

                    String data = object.getString(DataKey);
                    mData = ByteBuffer.wrap(Base64.decode(data, Base64.DEFAULT));
                }
            } catch (JSONException e) {
                Log.d(Tag, "Unable to convert JSON to RetrievePictureResponse");
                mError = true;
            }
        } else {
            Log.d(Tag, "Unable to convert NULL to RetrievePictureResponse");
            mError = true;
        }
    }

    public boolean hasError() {
        return mError;
    }

    public boolean wasValidated() {
        return mValid;
    }

    public ByteBuffer getData() {
        return mData;
    }
}
