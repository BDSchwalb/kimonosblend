package com.kimonosblend.app.networking;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class UsernamePasswordRequest implements ClientRequest {

    private static final String Tag = UsernamePasswordRequest.class.getName();

    private static final String UsernameKey = "username";
    private static final String PasswordKey = "password";

    private String mUsername;
    private String mPassword;


    public UsernamePasswordRequest(String username, String password) {
        mUsername = username;
        mPassword = password;
    }

    @Override
    public JSONObject createPayload() {
        JSONObject object = new JSONObject();
        try {
            object.put(UsernameKey, mUsername);
            object.put(PasswordKey, mPassword);
        } catch (JSONException e) {
            Log.d(Tag, "Unable to convert properties to JSON");
        }

        return object;
    }
}
