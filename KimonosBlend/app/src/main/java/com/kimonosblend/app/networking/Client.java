package com.kimonosblend.app.networking;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class Client {

    private static String Tag = Client.class.getName();

    private static String ServerAddress = "http://ec2-18-219-138-115.us-east-2.compute.amazonaws.com:3000";

    public static String Login = "/v1/login";
    public static String Register = "/v1/createaccount";

    public static String AddClothes = "/v1/createclothes";
    public static String RetrieveClothes = "/v1/clothes";
    public static String RetrievePictures = "/v1/pictures";

    private static CookieManager mCookieMgr = new CookieManager();

    static {
        CookieHandler.setDefault(mCookieMgr);
    }

    public JSONObject makeRequest(String path, ClientRequest request) {

        HttpURLConnection connection = null;
        JSONObject result = null;

        try {
            // Establish a connection with the server
            URL url = new URL(ServerAddress + path);

            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoInput(true);
            connection.setDoOutput(request != null);
            connection.connect();

            Log.v(Tag, "Connected");

            // Write the JSON object
            JSONObject requestJson = request.createPayload();
            if (requestJson != null) {
                byte[] sendData = requestJson.toString().getBytes();
                OutputStream os = connection.getOutputStream();
                os.write(sendData);
                os.close();
            }

            Log.v(Tag, "Wrote data");

            // Read the stream
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder builder = new StringBuilder();

            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                builder.append("\n");
            }

            Log.v(Tag, "Recieved data");

            result = new JSONObject(builder.toString());

        } catch (JSONException e) {
            Log.d(Tag, "makeRequest: Failed to parse JSON");
        } catch (ProtocolException e) {
            Log.d(Tag, "makeRequest: Protocol failure");
        } catch (MalformedURLException e) {
            Log.d(Tag, "makeRequest: Malformed URL");
        } catch (IOException e) {
            Log.d(Tag, "makeRequest: IOException");
        } finally {
            if (connection != null)
                connection.disconnect();
        }

        return result;
    }
}
