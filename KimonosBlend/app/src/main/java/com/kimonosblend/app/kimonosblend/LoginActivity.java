package com.kimonosblend.app.kimonosblend;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kimonosblend.app.networking.Client;
import com.kimonosblend.app.networking.UsernamePasswordRequest;
import com.kimonosblend.app.networking.ValidationResponse;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    // Keep track of this in case we need to cancel it
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private TextView mLoginStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        mUsernameView = findViewById(R.id.username);

        mPasswordView = findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button emailSignInButton = findViewById(R.id.sign_in_button);
        emailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        if (BuildConfig.DEBUG) {
            Button fastLoginButton = findViewById(R.id.fastLogin);
            fastLoginButton.setVisibility(View.VISIBLE);
            fastLoginButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    mUsernameView.setText("DebugAccount");
                    mPasswordView.setText("i28(8eeirkfu*3i(00038inuwuki**eifnviufurn");
                    attemptLogin();
                }
            });
        }

        mLoginStatus = findViewById(R.id.login_status);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        mLoginStatus.setText(R.string.status_validating);

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password
        if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Update the status indicator
            mLoginStatus.setText(R.string.status_contacting_server);

            // Start a background task to perform the user login attempt.
            mAuthTask = new UserLoginTask(username, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isUsernameValid(String username) {
        return username.length() >= 4;
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private String Tag = UserLoginTask.class.getName();

        private final String mUsername;
        private final String mPassword;

        public UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            Client client = new Client();
            UsernamePasswordRequest request = new UsernamePasswordRequest(mUsername, mPassword);

            if (login(client, request)) {
                return true;
            } else if (createAccount(client, request)) {
                return login(client, request);
            }

            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            mLoginStatus.setText("");

            if (success) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            mLoginStatus.setText("");
        }

        private boolean login(Client client, UsernamePasswordRequest request) {

            ValidationResponse response;

            // Try to authenticate account
            response = new ValidationResponse(client.makeRequest(Client.Login, request));

            // Interpret response
            if (!response.hasError()) {
                Log.d(Tag, response.getMessage());
                if (response.wasValidated()) {
                    return true;
                }
            } else {
                Log.d(Tag, "Error submitting data");
            }

            return false;
        }

        private boolean createAccount(Client client, UsernamePasswordRequest request) {

            ValidationResponse response;

            // Try to create the account
            response = new ValidationResponse(client.makeRequest(Client.Register, request));

            // Interpret response
            if (!response.hasError()) {
                Log.d(Tag, response.getMessage());
                if (response.wasValidated()) {
                    return true;
                }
            } else {
                Log.d(Tag, "Error submitting data");
            }

            return false;
        }
    }
}


