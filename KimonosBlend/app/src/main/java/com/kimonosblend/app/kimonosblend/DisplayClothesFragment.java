package com.kimonosblend.app.kimonosblend;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kimonosblend.app.networking.Client;
import com.kimonosblend.app.networking.ClientRequest;
import com.kimonosblend.app.networking.RetrieveClothingResponse;
import com.kimonosblend.app.strategy.ClothingAllStrategy;
import com.kimonosblend.app.strategy.ClothingNarrowStrategy;

import org.json.JSONObject;

public abstract class DisplayClothesFragment extends Fragment {

    private RecyclerView mListView;
    private LinearLayoutManager mLayoutMgr;
    private ListAdapter mAdapter;
    private RetrieveClothesTask mClothesTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wardrobe, container, false);

        mListView = view.findViewById(R.id.clothesRecyclerView);
        mListView.setHasFixedSize(false);

        mLayoutMgr = new LinearLayoutManager(getContext());
        mListView.setLayoutManager(mLayoutMgr);

        mClothesTask = new RetrieveClothesTask();
        mClothesTask.execute();

        return view;
    }

    public abstract ClothingNarrowStrategy getNarrowStrategy();

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView pictureView;
        public TextView nameView;
        public TextView categoryView;
        public TextView typeView;
        public TextView colorView;

        public ViewHolder(View view) {
            super(view);

            pictureView = view.findViewById(R.id.clothingPicture);
            nameView = view.findViewById(R.id.clothingName);
            categoryView = view.findViewById(R.id.clothingCategory);
            typeView = view.findViewById(R.id.clothingType);
            colorView = view.findViewById(R.id.clothingColor);
        }
    }

    private class ListAdapter extends RecyclerView.Adapter<ViewHolder> {

        RetrieveClothingResponse.Clothing[] mDataset;

        public ListAdapter(RetrieveClothingResponse.Clothing[] dataset) {
            mDataset = dataset;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // Create view from xml
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_element_clothing, parent, false);

            // Return the wrapper
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            RetrieveClothingResponse.Clothing clothing = mDataset[position];

            holder.nameView.setText(clothing.name);
            holder.categoryView.setText(clothing.category);
            holder.typeView.setText(clothing.type);
            holder.colorView.setText(clothing.color);

            Glide.with(getContext())
                    .load("pic_id:" + clothing.pictureResourceId)
                    .into(holder.pictureView);
        }

        @Override
        public int getItemCount() {
            return mDataset.length;
        }
    }

    private class RetrieveClothesTask extends
            AsyncTask<Void, Void, RetrieveClothingResponse.Clothing[]> {

        private final String Tag = RetrieveClothingResponse.class.getName();

        @Override
        protected RetrieveClothingResponse.Clothing[] doInBackground(Void... voids) {

            // No data to send
            ClientRequest request = new ClientRequest() {
                @Override
                public JSONObject createPayload() {
                    return new JSONObject();
                }
            };

            // Request resource
            Client client = new Client();
            RetrieveClothingResponse response = new RetrieveClothingResponse(
                    client.makeRequest(Client.RetrieveClothes, request));

            if (!response.hasError()) {
                if (response.wasValidated()) {
                    return response.getClothes();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(RetrieveClothingResponse.Clothing[] results) {
            mClothesTask = null;

            // Filter if needed
            if (results != null) {
                results = getNarrowStrategy().narrow(results);
            }

            // Display
            if (results != null) {
                Log.d(Tag, "Refreshing displayed clothes");
                mAdapter = new ListAdapter(results);
                mListView.setAdapter(mAdapter);
            } else {
                Log.d(Tag, "Nothing returned");
            }
        }

        @Override
        protected void onCancelled() {
            mClothesTask = null;
        }
    }
}
