package com.kimonosblend.app.networking;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class AddClothingRequest implements ClientRequest {

    private static final String Tag = AddClothingRequest.class.getName();

    private static final String NameKey = "name";
    private static final String CategoryKey = "category";
    private static final String TypeKey = "type";
    private static final String ColorKey = "color";
    private static final String ImageKey = "picture";

    private String mName;
    private String mCategory;
    private String mType;
    private String mColor;
    private File mImage;

    public AddClothingRequest(String name, String category, String type, String color, File image) {
        mName = name;
        mCategory = category;
        mType = type;
        mColor = color;
        mImage = image;
    }

    @Override
    public JSONObject createPayload() {
        JSONObject object = new JSONObject();
        try {
            object.put(NameKey, mName);
            object.put(CategoryKey, mCategory);
            object.put(TypeKey, mType);
            object.put(ColorKey, mColor);

            if (mImage.exists()) {
                int size = (int) mImage.length();
                byte[] bytes = new byte[size];
                BufferedInputStream stream = new BufferedInputStream(new FileInputStream(mImage));
                stream.read(bytes, 0, bytes.length);

                String encodedImage = Base64.encodeToString(bytes, Base64.DEFAULT);
                object.put(ImageKey, encodedImage);
            }
        } catch (JSONException e) {
            Log.d(Tag, "Unable to convert properties to JSON");
        } catch (FileNotFoundException e) {
            Log.d(Tag, "Image does not exist");
        } catch (IOException e) {
            Log.d(Tag, "Error while reading image");
        }

        return object;
    }
}
