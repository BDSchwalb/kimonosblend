package com.kimonosblend.app.networking;

import org.json.JSONObject;

public interface ClientRequest {
    JSONObject createPayload();
}
