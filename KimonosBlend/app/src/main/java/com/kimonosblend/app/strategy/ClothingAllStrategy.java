package com.kimonosblend.app.strategy;

import com.kimonosblend.app.networking.RetrieveClothingResponse.Clothing;

public class ClothingAllStrategy implements ClothingNarrowStrategy {
    @Override
    public Clothing[] narrow(Clothing[] clothing) {
        return clothing;
    }
}
