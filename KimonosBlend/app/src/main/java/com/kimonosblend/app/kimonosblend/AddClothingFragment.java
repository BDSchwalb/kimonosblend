package com.kimonosblend.app.kimonosblend;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.kimonosblend.app.networking.AddClothingRequest;
import com.kimonosblend.app.networking.Client;
import com.kimonosblend.app.networking.ValidationResponse;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;
import static com.kimonosblend.app.kimonosblend.MainActivity.REQUEST_IMAGE_CAPTURE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddClothingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddClothingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddClothingFragment extends Fragment {

    private static final String Tag = AddClothingFragment.class.getName();

    private OnFragmentInteractionListener mListener;

    private EditText mNameField;
    private EditText mColorField;

    private Spinner mCategorySpinner;
    private Spinner mTypeSpinner;

    private ImageButton mPictureButton;
    private Button mAddButton;

    private TextView mStatusText;

    private String mCurrentPhotoPath;

    SubmitClothingTask mSubmitTask;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddClothingFragment.
     */
    public static AddClothingFragment newInstance() {
        AddClothingFragment fragment = new AddClothingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    /**
     * Public default ctor. Required, does not do anything.
     */
    public AddClothingFragment() {
        mCurrentPhotoPath = "";
    }

    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        // TODO
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_clothing, container, false);

        mNameField = view.findViewById(R.id.clothingName);
        mColorField = view.findViewById(R.id.clothingColor);

        mCategorySpinner = view.findViewById(R.id.clothingCategory);
        mTypeSpinner = view.findViewById(R.id.clothingType);

        mPictureButton = view.findViewById(R.id.pictureButton);
        mPictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhoto();
            }
        });

        mAddButton = view.findViewById(R.id.addClothingButton);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitData();
            }
        });

        mStatusText = view.findViewById(R.id.submissionStatus);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityResult(int code, int result, Intent data) {
        if (code == REQUEST_IMAGE_CAPTURE && result == RESULT_OK) {
            Bitmap image = BitmapFactory.decodeFile(mCurrentPhotoPath);
            mPictureButton.setImageBitmap(image);
        }
    }

    private void submitData() {
        if (mSubmitTask == null) {
            mStatusText.setText(R.string.status_contacting_server);

            mSubmitTask = new SubmitClothingTask();
            mSubmitTask.execute();
        }
    }

    private void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            try {
                File photoFile = createImageFile();
                mCurrentPhotoPath = photoFile.getAbsolutePath();

                Uri photoUri = FileProvider.getUriForFile(getContext(),
                        "com.kimonosblend.app.FileProvider", photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            } catch (IOException e) {
                Log.d(Tag, "Failed to create an image file");
            }

            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File createImageFile() throws IOException {
        // Setup
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        return File.createTempFile(imageFileName,".jpg", storageDir);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SubmitClothingTask extends AsyncTask<Void, Void, Boolean> {

        private String mName;
        private String mCategory;
        private String mType;
        private String mColor;
        private String mPicturePath;

        public SubmitClothingTask() {
            mName = mNameField.getText().toString();
            mCategory = mCategorySpinner.getSelectedItem().toString();
            mType = mTypeSpinner.getSelectedItem().toString();
            mColor = mColorField.getText().toString();
            mPicturePath = mCurrentPhotoPath;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            // Setup
            Client client = new Client();

            File image = new File(mPicturePath);
            AddClothingRequest request = new AddClothingRequest(
                    mName, mCategory, mType, mColor, image);
            ValidationResponse response;

            // Send the data
            response= new ValidationResponse(client.makeRequest(Client.AddClothes, request));

            // Handle response
            if (!response.hasError()) {
                Log.d(Tag, response.getMessage());
                if (response.wasValidated()) {
                    return true;
                }
            } else {
                Log.d(Tag, "Error submitting data");
            }

            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mSubmitTask = null;

            if (success) {
                mStatusText.setText(R.string.status_success);

                mPictureButton.setImageResource(R.drawable.ic_menu_camera);
                mNameField.setText("");
                mColorField.setText("");
            } else {
                mStatusText.setText(R.string.status_failure);
            }
        }

        @Override
        protected void onCancelled() {
            mSubmitTask = null;
            mStatusText.setText("");
        }
    }
}
