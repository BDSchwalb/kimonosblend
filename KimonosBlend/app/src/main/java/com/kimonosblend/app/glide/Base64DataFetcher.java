package com.kimonosblend.app.glide;

import android.support.annotation.NonNull;
import android.util.Log;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import com.kimonosblend.app.networking.Client;
import com.kimonosblend.app.networking.RetrievePictureRequest;
import com.kimonosblend.app.networking.RetrievePictureResponse;

import java.nio.ByteBuffer;

public class Base64DataFetcher implements DataFetcher<ByteBuffer> {

    private static final String Tag = Base64DataFetcher.class.getName();

    private String mModel;

    public Base64DataFetcher(String model) {
        mModel = model;
    }

    @Override
    public void loadData(@NonNull Priority priority, @NonNull DataCallback<? super ByteBuffer> callback) {
        Client client = new Client();
        RetrievePictureRequest request = new RetrievePictureRequest(getModelId());

        RetrievePictureResponse response = new RetrievePictureResponse(
                client.makeRequest(Client.RetrievePictures, request));

        if (!response.hasError() && response.wasValidated()) {
            callback.onDataReady(response.getData());
        } else {
            Log.d(Tag, "Invalid response when retrieving " + getModelId());
            callback.onLoadFailed(new Exception("Failed to load: " + getModelId()));
        }
    }

    @Override
    public void cleanup() {

    }

    @Override
    public void cancel() {

    }

    @NonNull
    @Override
    public Class<ByteBuffer> getDataClass() {
        return ByteBuffer.class;
    }

    @NonNull
    @Override
    public DataSource getDataSource() {
        return DataSource.REMOTE;
    }

    private String getModelId() {
        int idStart = mModel.indexOf(':') + 1;
        return mModel.substring(idStart);
    }
}
