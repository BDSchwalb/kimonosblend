package com.kimonosblend.app.strategy;

import com.kimonosblend.app.networking.RetrieveClothingResponse.Clothing;
import com.kimonosblend.app.networking.RetrievePictureResponse;

public interface ClothingNarrowStrategy {
    /*
     * This method is passed an array of clothes and must return an array back.
     */
    public Clothing[] narrow(Clothing[] clothing);
}
