package com.kimonosblend.app.networking;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class ValidationResponse {

    public ValidationResponse(JSONObject object) {
        mError = false;
        mValid = false;
        mMsg = null;

        if (object != null) {
            try {
                if (object.has(ValidKeyAlt))
                    mValid = object.getBoolean(ValidKeyAlt);
                else
                    mValid = object.getBoolean(ValidKey);

                mMsg = object.getString(MsgKey);
            } catch (JSONException e) {
                Log.d(Tag, "Unable to convert JSON to ValidationResponse");
                mError = true;
            }
        } else {
            Log.d(Tag, "Unable to convert NULL to ValidationResponse");
            mError = true;
        }
    }

    public boolean hasError() {
        return mError;
    }

    public boolean wasValidated() {
        return mValid;
    }

    public String getMessage() {
        return mMsg;
    }

    private static final String Tag = ValidationResponse.class.getName();

    private static final String ValidKey = "valid";
    private static final String ValidKeyAlt = "success";
    private static final String MsgKey = "msg";

    private boolean mError;
    private boolean mValid;
    private String mMsg;
}