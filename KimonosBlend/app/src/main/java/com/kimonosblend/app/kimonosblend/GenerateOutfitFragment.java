package com.kimonosblend.app.kimonosblend;

import com.kimonosblend.app.strategy.ClothingNarrowStrategy;
import com.kimonosblend.app.strategy.ClothingPickOutfitStrategy;

public class GenerateOutfitFragment extends DisplayClothesFragment {

    private ClothingNarrowStrategy mStrategy = new ClothingPickOutfitStrategy();

    public static GenerateOutfitFragment newInstance() {
        GenerateOutfitFragment fragment = new GenerateOutfitFragment();
        return fragment;
    }

    @Override
    public ClothingNarrowStrategy getNarrowStrategy() {
        return mStrategy;
    }
}
