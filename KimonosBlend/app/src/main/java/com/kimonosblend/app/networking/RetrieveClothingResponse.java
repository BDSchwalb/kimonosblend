package com.kimonosblend.app.networking;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RetrieveClothingResponse {

    private static final String Tag = RetrieveClothingResponse.class.getName();

    private static final String ValidKey = "valid";

    private static final String ClothingKey = "clothing";

    private static final String NameKey = "name";
    private static final String CategoryKey = "category";
    private static final String TypeKey = "type";
    private static final String ColorKey = "color";
    private static final String PictureResourceIdKey = "picture";

    private boolean mError;
    private boolean mValid;
    private ArrayList<Clothing> mClothes;

    public RetrieveClothingResponse(JSONObject object) {
        mError = false;
        mValid = false;
        mClothes = new ArrayList<>();

        if (object != null) {
            try {
                if (!object.has(ValidKey) || object.getBoolean(ValidKey)) {
                    mValid = true;

                    JSONArray array = object.getJSONArray(ClothingKey);
                    for (int i = 0; i < array.length(); ++i) {
                        // Get object at index
                        JSONObject json = array.getJSONObject(i);

                        // Retrieve values
                        Clothing clothing = new Clothing();
                        clothing.name = json.getString(NameKey);
                        clothing.category = json.getString(CategoryKey);
                        clothing.type = json.getString(TypeKey);
                        clothing.color = json.getString(ColorKey);
                        clothing.pictureResourceId = json.getString(PictureResourceIdKey);

                        // Add to collection
                        mClothes.add(clothing);
                    }
                } else {
                    mValid = false;
                }
            } catch (JSONException e) {
                Log.d(Tag, "Failed to convert JSON to RetrieveClothingResponse");
                mError = true;
            }
        } else {
            Log.d(Tag, "Unable to convert NULL to RetrieveClothingResponse");
            mError = true;
        }
    }

    public boolean hasError() {
        return mError;
    }

    public boolean wasValidated() {
        return mValid;
    }

    public Clothing[] getClothes() {
        Clothing[] clothes = new Clothing[mClothes.size()];
        return mClothes.toArray(clothes);
    }

    public class Clothing {
        public String name;
        public String category;
        public String type;
        public String color;
        public String pictureResourceId;
    }
}
