const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Setup
const url = 'mongodb://localhost:27017';
const dbName = 'Test';

// Connect to the database
MongoClient.connect(url, function(err, client) {
    // Handle connection error
    if (err) {
        console.log("Failed to connect to server");
        if (client) {
            client.close();
        }
        process.exit(1);
    }

    // Established connection, get the database object`
    console.log("Connected successfully to server");
    const db = client.db(dbName);

    // Insert a document
    db.collection('inserts').insertOne({a:1}, function(err, r) {
        assert.equal(null, err);
        assert.equal(1, r.insertedCount);
    });

    // Insert multiple documents
    db.collection('inserts').insertMany([{a:2}, {a:3}], function(err, r) {
        assert.equal(null, err);
        assert.equal(2, r.insertedCount);
    });

    // Close connection
    client.close();
});
