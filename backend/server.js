import bodyParser from 'body-parser';
import express from 'express';
import session from 'express-session';
import mongoose from 'mongoose';
import morgan from 'morgan';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

import routerV1 from '~/controllers/v1/router';
import Clothes from '~/models/clothes.js';
import User from '~/models/user.js';

// Database
mongoose.connect('mongodb://localhost/clothes');

// Express server
const app = express();

// Logger (to console)
app.use(morgan('combined'));

// Passport setup
passport.use(new LocalStrategy(
    function(username, password, done){
        console.log(`Login attempt by ${username}`);
        User.findOne({ username: username }, function(err, user) {
            if (err) return done(err);
            if (!user) return done(null, false);

            // Compare against hashed password
            user.comparePassword(password, function(err, isMatch) {
                if (err) return done(err);
                return done(err, isMatch ? user : false);
            });
        });
    }
));
passport.serializeUser((user, done) => done(null, user.username));
passport.deserializeUser((username, done) => {
    User.findOne({ username: username}, (err, user) => done(err, user));
});

app.use(session({ secret: "832ir35i1ekir3562eikw" }));
app.use(bodyParser.urlencoded({ extended: false, limit: '8mb' }));
app.use(bodyParser.json({ limit: '8mb' }));

// Integrate passport with express
app.use(passport.initialize());
app.use(passport.session());

// v1 access
app.use('/v1/', routerV1);

// Launch server
const apiServer = app.listen(3000, () => {
  const info = apiServer.address();
  console.log(`Listening at http://${info.address}:${info.port}`);
});
