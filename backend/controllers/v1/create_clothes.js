import mongoose from 'mongoose';

import Clothes from '~/models/clothes';
import Pictures from '~/models/pictures';

export const createClothesIndex = (req, res) => {
  // Responses
  const Success = JSON.stringify({ valid: true,  msg: 'Clothes added' });
  const Failure = JSON.stringify({ valid: false, msg: 'Clothes not added' });

  // Check if the user is logged in
  if (!req.user) {
    console.log("Attempt to create clothes while not logged in");
    return res.send(Failure);
  }

  // Convert body to JSON
  let picId = new mongoose.Types.ObjectId();
  let pictureData = {
    _id: picId,
    owner: req.user.id,
    data: req.body.picture
  };

  let data = {
    owner: req.user.id,
    name: req.body.name,
    type: req.body.type,
    color: req.body.color,
    category: req.body.category,
    picture: picId
  };

  // Save to the database
  let picture = new Pictures(pictureData);
  picture.save((err) => {
    if (err) console.log('Failed to save image');
  });

  let clothes = new Clothes(data);
  clothes.save((err) => err ? res.send(Failure) : res.send(Success));
}
