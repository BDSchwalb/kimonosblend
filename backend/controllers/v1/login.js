import passport from 'passport';

export const loginIndex = (req, res, next) => {
  passport.authenticate('local', (error, user, info) => {
    // Responses
    const Success = JSON.stringify({ valid: true, msg: 'authenticated' });
    const Failure = JSON.stringify({ valid: false, msg: 'failed authentication' });

    // Check for error
    if (error) { return res.send(Failure); }

    // Make sure a user exists
    if (!user) { return res.send(Failure); }

    // Validate
    req.logIn(user, (err) => err ? res.send(Failure) : res.send(Success));
  })(req, res, next);
}
