import Pictures from '~/models/pictures';

export const picturesIndex = (req, res) => {
  // Common responses
  const Failure = JSON.stringify({ valid: false });

  // Check user was authenticated
  if (!req.user) {
    console.log("Attempt to retrieve pictures while not logged in");
    return res.send(Failure);
  }

  // Extract data
  const UserId = req.user.id;
  const PictureId = req.body.id;

  // Find relevant data
  Pictures.findById(PictureId)
    .where('owner', UserId)
    .exec((err, picture) => {
      if (picture) res.json({ valid: true, data: picture.data.toString('ascii') });
      else res.send(Failure);
    });

  // Return results
}
