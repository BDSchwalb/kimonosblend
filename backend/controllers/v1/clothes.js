import Clothes from '~/models/clothes';

export const clothesIndex = (req, res) => {
  // Check that the user is authenticated
  if (!req.user) {
    console.log("Attempt to retrieve pictures while not logged in");
    return res.json({ valid: false });
  }

  const UserId = req.user.id;

  // Retrieve the entire list
  Clothes.find({ owner: UserId })
    .lean()
    .exec((err, clothes) => {
      if (err) res.json({ valid: false });
      else res.json({ clothing: clothes });
    });
}
