import User from '~/models/user';

export const createAccountIndex = (req, res) => {
  // Responses
  const Success = JSON.stringify({ valid: true,  msg: 'Account created'  });
  const Failure = JSON.stringify({ valid: false, msg: 'Unable to create' });

  // Convert the body to a json object
  let data = {
    username: req.body.username,
    password: req.body.password
  };

  // Attempt to create an account
  const user = new User(data);
  user.save((err) => err ? res.send(Failure) : res.send(Success) );
}
