import { Router } from 'express';
import { createAccountIndex } from '~/controllers/v1/create_account';
import { createClothesIndex } from '~/controllers/v1/create_clothes';
import { clothesIndex } from '~/controllers/v1/clothes';
import { loginIndex } from '~/controllers/v1/login';
import { picturesIndex } from '~/controllers/v1/picture';

const routerV1 = Router();

routerV1.route('/createaccount').post(createAccountIndex);
routerV1.route('/login').post(loginIndex);

routerV1.route('/createclothes').post(createClothesIndex);
routerV1.route('/clothes').post(clothesIndex);
routerV1.route('/pictures').post(picturesIndex);

export default routerV1;
