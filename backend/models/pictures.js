import mongoose, { Schema } from 'mongoose';

// Define a picture schema
var pictureSchema = new Schema({
  owner: { type: Schema.Types.ObjectId, required: true, index: true },
  data: { type: Buffer, required: true }
});

export default mongoose.model('pictures', pictureSchema);
