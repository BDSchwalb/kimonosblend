import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';

// Define a user schema
var userSchema = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true }
});

userSchema.pre('save', function(next) {
    var user = this;

    // Only hash if needed
    if (!user.isModified('password')) return next();

    // Compute hash
    bcrypt.hash(user.password, 12, function(err, hash) {
        if (err) return next(err);

        // Store result in  the database
        user.password = hash;
        next();
    });
})

userSchema.methods.comparePassword = function(candidate, done) {
    bcrypt.compare(candidate, this.password, function(err, isMatch) {
        if (err) return done(error);
        done(null, isMatch);
    });
}

export default mongoose.model('users', userSchema);
