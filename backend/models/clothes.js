import mongoose, { Schema } from 'mongoose';

// Define the clothes schema
var clothesSchema = new Schema({
    owner: { type: Schema.Types.ObjectId, required: true, index: true },
    name: { type: String, required: true },
    type: { type: String, required: true, index: true },
    color: { type: String, required: true, index: true },
    category: { type: String, required: true, index: true },
    picture: { type: Schema.Types.ObjectId, required: false }
});

export default mongoose.model('clothes', clothesSchema);
