import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  AppRegistry,
  Image,
  TextInput,
  Button,
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';

export default class AddClothes extends React.Component {

    constructor(props) {
      super(props);
      this.main=this.main.bind(this);
      this.state = {name: '', color: '', category: '', type: ''};
      this.submit=this.submit.bind(this);
    }

    main(){
     this.props.changePage('MainPage');
    }

    submit(){
      let data = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          name: this.state.name,
          type: this.state.type,
          color: this.state.color,
          category: this.state.category
        })
      };

      let address = global.SERVER_ADDRESS + '/createclothes';
      fetch(address, data)
        .then(response => response.json())
        .then(json => {
          console.log(json);
        })
        .catch(error => {
          console.log(error);
        });
    }

    render() {
      let category_data = [{
        value: 'Formal',
      }, {
        value: 'Semi-Formal',
      }, {
        value: 'Casual',
      }];

      let type_data = [{
        value: 'Short Sleve Shirt',
      }, {
        value: 'Long Sleve Shirt',
      }, {
        value: 'Suit Coat',
      }, {
        value: 'Tank Top'
      }, {
        value: 'Collared Shirt'
      }, {
        value: 'Shorts'
      }, {
        value: 'Jeans'
      }, {
        value: 'Dress Pants'
      }, {
        value: 'Sweat Pants'
      }];

        return (
         <View>
            <Text style={{margin: 15}}>
                   Add Clothes page
            </Text>
            <Button
                 onPress={this.main}
                 title="Back to Main Page"/>
            <TextField
                 value={this.state.name}
                 onChangeText={(text) => this.setState({name: text})}
                 label='Name'
               />
            <Dropdown
                 label='Category'
                  data={category_data}
                  onChangeText={(value, index, data) => this.setState({category: value})}
             />
            <Dropdown
                 label='Type'
                  data={type_data}
                  onChangeText={(value, index, data) => this.setState({type: value})}
             />
             <TextField
                 value={this.state.color}
                 onChangeText={(text) => this.setState({color: text})}
                 label='Color'
               />
               <Button
                 onPress={this.submit}
                 title="Add to database"/>
         </View>
      );
    }
  }
