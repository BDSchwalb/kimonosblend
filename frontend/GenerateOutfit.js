import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  StatusBar,
  AppRegistry,
  Image,
  TextInput,
  Button 
} from 'react-native';

export default class AddClothes extends React.Component {
    constructor(props) {
        super(props);
        this.main=this.main.bind(this);
      }
    
      main(){
         this.props.changePage('MainPage');
         
      }
    render() {
        return (
         <View>
            <Text style={{margin: 15}}>
                   Generate Outfit page
            </Text>
            <Button
                 onPress={this.main}
                 title="Back to Main Page"/>
         </View>
      );
    }
  } 
  