import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  AppRegistry,
  Image,
  TextInput,
  Button
} from 'react-native';

export default class LoginPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = { username: '', password: '' };
    this.login = this.login.bind(this);
    this.createAccount = this.createAccount.bind(this);
  }

  login(){
    console.log('Attempting to login');

    let data = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    };

    let address = global.SERVER_ADDRESS + '/login';
    fetch(address, data)
      .then(response => response.json())
      .then(json => {
        console.log('Login response');
        console.log(json);

        // Check if we can head to the main page
        if (json.valid) {
          this.props.changePage('MainPage');
        }
      })
      .catch(error => {
        //showe error message if cant login
        console.log(error);
      });
  }

  createAccount() {
    console.log('Attempting to create account');

    // Data to send
    let data = {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password
      })
    };

    // Attempt to create the account on the server
    let address = global.SERVER_ADDRESS + '/createaccount';
    fetch(address, data)
      .then(response => response.json())
      .then(json => {
        console.log('Account creation response');
        console.log(json);
      })
      .catch(error => {
        //shower error message here if cant register
        console.log(error);
      });
  }

  render() {
      let pic = {
        uri: 'https://upload.wikimedia.org/wikipedia/commons/a/a7/Walk_In_Closet_-_Expandable_Closet_Rod_and_Shelf.jpg'
      };
      return (
        <View>
          <Image source={pic} style={{width: 500, height: 400}}/>
          <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', margin: 10}}>
            <Text style={{margin: 10}}>
              User Name:
            </Text>
            <TextInput
              style={{height: 40, width: 200, borderColor: 'gray', borderWidth: 1}}
              onChangeText={(text) => this.setState({username: text})}
              value={this.state.text}
            />
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center',}}>
          <Text style={{margin: 15}}>
            Password:
          </Text>
          <TextInput
            style={{height: 40, width: 200, borderColor: 'gray', borderWidth: 1}}
            onChangeText={(text) => this.setState({password: text})}
            value={this.state.text}
            secureTextEntry={true}
          />
        </View>
        <Button
          onPress={this.login}
          title="LOGIN"/>
        <Button
          onPress={this.createAccount}
          title="REGISTER"/>
      </View>



    );
  }
}
