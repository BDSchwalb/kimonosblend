import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  AppRegistry,
  Image,
  TextInput,
  Button
} from 'react-native';

import MainPage from './MainPage';
import LoginPage from './LoginPage';
import AddClothes from './AddClothes';
import ManagePage from './ManagePage';
import GenerateOutfit from './GenerateOutfit';

global.SERVER_ADDRESS="http://ec2-18-219-138-115.us-east-2.compute.amazonaws.com:3000";

export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state={componentSelected: 'MainPage'};
    this.changePage=this.changePage.bind(this);
  }

  changePage(Page){
    this.setState({componentSelected: Page});
  }

  renderPage(Page){
    if(Page == 'LoginPage') {
      return <LoginPage changePage={this.changePage} />
    }
    else if(Page == 'MainPage') {
      return <MainPage changePage={this.changePage} />
    }
    else if (Page == 'ManagePage') {
      return <ManagePage changePage={this.changePage} />
    }
    else if(Page == 'AddClothes') {
      return <AddClothes changePage={this.changePage} />
    }
    else if(Page == 'GenerateOutfit') {
      return <GenerateOutfit changePage={this.changePage} />
    }
  }

  render() {
      return (
        <View>
          {this.renderPage(this.state.componentSelected)}
        </View>
    );
  }
}
