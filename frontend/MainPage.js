import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  AppRegistry,
  Image,
  TextInput,
  Button
} from 'react-native';

export default class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.addClothes=this.addClothes.bind(this);
        this.manageClothes=this.manageClothes.bind(this);
        this.genOut=this.genOut.bind(this);
        this.logOut=this.logOut.bind(this);
      }

      addClothes(){
         this.props.changePage('AddClothes');
      }

      manageClothes() {
        this.props.changePage('ManagePage');
      }

      genOut(){
        this.props.changePage('GenerateOutfit');
     }

     logOut(){
        this.props.changePage('LoginPage');
     }

    render() {


        return (
         <View>
            <Text style={{margin: 15}}>
                   WELCOME
            </Text>
            <Button
                 onPress={this.addClothes}
                 title="Add Clothes"/>
            <Button
                  onPress={this.manageClothes}
                  title="Manage Clothes"/>
            <Button
                 onPress={this.genOut}
                 title="Generate Outfit"/>
             <Button
                 onPress={this.logOut}
                 title="Logout"/>
         </View>
      );
    }
  }
