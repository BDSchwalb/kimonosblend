import React from 'react';
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  View,
} from 'react-native';

styles = StyleSheet.create({
  container: {
    borderStyle: "solid",
    borderWidth: 1
  }
});

export default class ManagePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: "Hello World!" };
    this.mainPage = this.mainPage.bind(this);

    let data = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
    };

    let address = global.SERVER_ADDRESS + '/v1/clothes.json';
    fetch(address, data)
      .then(response => response.json())
      .then(json => {
        console.log("Got data");
        console.log(json);
        let i = 0;
        this.setState({ data: json.map(entry => { return { ...entry, key: i++ } } )});
      })
      .catch(error => {
        console.log(error);
      });
  }

  mainPage() {
    this.props.changePage('MainPage');
  }

  render() {
    return (
      <View>
        <Text style={{margin: 15}}>
          Manage Clothes page
        </Text>
        <Button
            onPress = {this.mainPage}
            title="Back to Main Page"/>
        <FlatList
            data = {this.state.data}
            renderItem = { ({ item }) => {
              return (
                <View style={styles.container} >
                  <Text>Name: {item.name}</Text>
                  <Text>Type: {item.type}</Text>
                  <Text>Color: {item.color}</Text>
                  <Text>Category: {item.category}</Text>
                </View>
              );
            }} />
      </View>
    );
  }
}
